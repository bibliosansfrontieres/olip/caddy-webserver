# caddy-webserver

[![pipeline status](https://gitlab.com/bibliosansfrontieres/olip/caddy-webserver/badges/master/pipeline.svg)](https://gitlab.com/bibliosansfrontieres/olip/caddy-webserver/commits/master)


a powerful, enterprise-ready, open source web server with automatic HTTPS written in Go

## How to use it ? 

This webserver is intended to run on OLIP plateform. It allows to serve any kind of static website stored under `/data/content` folder.
HTML files must be sored at the root of the archive file. 